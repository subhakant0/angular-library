import { TestBed } from '@angular/core/testing';

import { Login08Service } from './login08.service';

describe('Login08Service', () => {
  let service: Login08Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Login08Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
