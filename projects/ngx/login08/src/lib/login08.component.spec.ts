import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Login08Component } from './login08.component';

describe('Login08Component', () => {
  let component: Login08Component;
  let fixture: ComponentFixture<Login08Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Login08Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Login08Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
