import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
export interface ErrorMessage {
  [key: string]: { [key: string]: string };
}

export interface LoginConfiguration {
  hideRememberMe?: boolean;
  hideSocialLogin?: boolean;
  hideFacebookLogin?: boolean;
  hideTwitterLogin?: boolean;
  hideGoogleLogin?: boolean;
  hideForgotPassword?: boolean;
  signUpEnabled?: boolean;
}
@Component({
  selector: 'ngx-login08',
  templateUrl: './login08.component.html',
  styleUrls: ['./login08.component.scss'],
})
export class Login08Component implements OnInit {
  @Input() title = 'App';
  @Input() description;
  @Input() loginForm;
  @Input() errorMessage: ErrorMessage;
  @Input() disableLogin = false;
  @Input() config: LoginConfiguration;
  @Output() onSubmit = new EventEmitter();
  @Output() onInputChange = new EventEmitter();
  constructor(private formBuilder: FormBuilder) {
    // This can be passed dynamically through the @Input
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      rememberMe: [false],
    });
    // This can be passed dynamically through the @Input
    this.errorMessage = {
      username: {
        required: 'User name is required',
      },
      password: {
        required: 'Password is required',
      },
    };
    // This can be passed dynamically through the @Input
    // this.onInputChange = new EventEmitter();
  }

  ngOnInit(): void {
    if (this.onInputChange) {
      this.loginForm.valueChanges.subscribe(() => {
        this.onInputChange.emit(this.loginForm.value);
      });
    }
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  public login() {
    this.onSubmit.emit(this.loginForm.value);
  }
}
