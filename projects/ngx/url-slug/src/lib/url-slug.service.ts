import { Injectable } from '@angular/core';
import { CAMELCASE_REGEXP_PATTERN, validate } from './url-slug/common';
import { replace } from './url-slug/dictionary';
import { LOWERCASE_TRANSFORMER } from './url-slug/transformers';

const REVERT = /[^-._~!$&'()*+,;=]+/g;

const REVERT_CAMELCASE = new RegExp(
  "[^-._~!$&'()*+,;=]*?" + CAMELCASE_REGEXP_PATTERN + "|[^-._~!$&'()*+,;=]+",
  'g'
);

const REVERT_CAMELCASE_ONLY = new RegExp(
  '.*?' + CAMELCASE_REGEXP_PATTERN + '|.+',
  'g'
);

// eslint-disable-next-line no-misleading-character-class
const COMBINING_CHARS = /[\u0300-\u036F\u1AB0-\u1AFF\u1DC0-\u1DFF]+/g;

const CONVERT = /[A-Za-z\d]+/g;

const CONVERT_CAMELCASE = new RegExp(
  '[A-Za-z\\d]*?' + CAMELCASE_REGEXP_PATTERN + '|[A-Za-z\\d]+',
  'g'
);

export declare type Options = {
  camelCase?: boolean;
  dictionary?: object;
  separator?: string;
  transformer?: false | Transformer;
};

export declare type RevertOptions = Omit<Options, 'separator'> & {
  separator?: null | string;
};

export declare type Transformer = (
  fragments: string[],
  separator: string
) => string;

@Injectable({
  providedIn: 'root',
})
export class UrlSlugService {
  constructor() {}

  public convert(string: string, options?: Options): string {
    options = options || {};

    validate(options);

    const camelCase =
      options.camelCase !== undefined ? options.camelCase : true;

    const separator = options.separator !== undefined ? options.separator : '-';

    const transformer =
      options.transformer !== undefined
        ? options.transformer
        : LOWERCASE_TRANSFORMER;

    const fragments = (
      options.dictionary
        ? replace(String(string), options.dictionary)
        : String(string)
    )
      .normalize('NFKD')
      .replace(COMBINING_CHARS, '')
      .match(camelCase ? CONVERT_CAMELCASE : CONVERT);

    if (!fragments) {
      return '';
    }

    return transformer
      ? transformer(fragments, separator)
      : fragments.join(separator);
  }

  public revert(slug: string, options?: RevertOptions): string {
    options = options || {};

    validate(options, { separator: null });

    const camelCase =
      options.camelCase !== undefined ? options.camelCase : false;

    const separator = options.separator;

    const transformer =
      options.transformer !== undefined ? options.transformer : false;

    let fragments;
    slug = String(slug);

    /* Determine which method will be used split the slug */

    if (separator === '') {
      fragments = camelCase
        ? slug.match(REVERT_CAMELCASE_ONLY)
        : [String(slug)];
    } else if (typeof separator === 'string') {
      fragments = slug.split(separator);
    } else {
      fragments = slug.match(camelCase ? REVERT_CAMELCASE : REVERT);
    }

    if (!fragments) {
      return '';
    }

    return transformer ? transformer(fragments, ' ') : fragments.join(' ');
  }
}
