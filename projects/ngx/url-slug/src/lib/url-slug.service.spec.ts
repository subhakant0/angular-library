import { TestBed } from '@angular/core/testing';

import { UrlSlugService } from './url-slug.service';

describe('UrlSlugService', () => {
  let service: UrlSlugService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlSlugService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
