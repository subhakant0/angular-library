/*
 * Public API Surface of url-slug
 */

export * from './lib/url-slug.service';
export * from './lib/url-slug/transformers';