/*
 * Public API Surface of angular-seo
 */

export { JsonLdService, JsonLdModule } from './lib/json-ld/index';
export { SeoSocialShareData, SeoSocialShareService, NgxSeoMetaTag, NgxSeoMetaTagAttr } from './lib/seo-social-share/index';
