// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { Login08Component } from 'projects/ngx/login08/src/public-api';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';

export default {
  title: 'Login/Login08',
  component: Login08Component,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCheckboxModule,
      ],
    }),
  ],
} as Meta;

const Template: Story<Login08Component> = (args: Login08Component) => ({
  component: Login08Component,
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {
  title: 'DyWeb',
  description: 'Create you blog with DyWeb',
};

export const DisabledSocialLogin = Template.bind({});
DisabledSocialLogin.args = {
  title: 'DyWeb',
  description: 'Create you blog with DyWeb',
  config: {
    hideSocialLogin: true
  }
};

export const DisabledRememberMe = Template.bind({});
DisabledRememberMe.args = {
  title: 'DyWeb',
  description: 'Create you blog with DyWeb',
  config: {
    hideRememberMe: true
  }
};

export const CustomErrorMessage = Template.bind({});
CustomErrorMessage.args = {
  title: 'DyWeb',
  description: 'Create you blog with DyWeb',
  errorMessage: {
    username: {
      required: 'This is a custom error message for username required',
    },
    password: {
      required: 'This is a custom error message for password required',
    },
  }
};